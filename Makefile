.PHONY: osx linux windows

osx:
	g++ -g3 -std=c++11 -Wno-narrowing -Iinclude -Llib/osx glad.c main.cpp -o main -lglfw3 -framework Cocoa -framework IOKit -framework CoreFoundation -framework CoreVideo
	./main

linux:
	g++ -g3 -std=c++11 -Wno-narrowing -Iinclude -Llib/linux glad.c main.cpp -o main -lglfw3 -lrt -lm -ldl -lX11 -lpthread -lXrandr -lXinerama -lXxf86vm -lXcursor -lGL
	./main

windows:
	g++ -g3 -std=c++11 -Wno-narrowing -Iinclude -Llib/windows glad.c main.cpp -o main.exe -lglfw3 -lopengl32 -lgdi32
	./main.exe
