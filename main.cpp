#include "glad/glad.h"
#include "glfw3.h"
#include <cstring>
#include <iostream>

using namespace std;

/*
 * Shader source code.
 */

// This is the source code for the shaders we want to use. We could load them from files, but it's easier
// to just put them here.

// This is our vertex shader. Vertex shaders operate on individual vertices.
// Vertex shaders MUST set the gl_Position variable to something. Here, we set the X and Y of the position
// to the pos we get from the vertex data.
const char* VERTEX_SHADER = R"(
    #version 330
    layout(location = 0) in vec2 pos;

    void main() {
        gl_Position = vec4(pos, 0, 1);
    }
)";

// This is our fragment shader. Fragment shaders operate on every pixel in every polygon. Fragment shaders
// MUST output a vec4 color, which will be the color of the pixel. Here we just set every pixel to white,
// so whatever polygons we draw will be solid white.
const char* FRAGMENT_SHADER = R"(
    #version 330
    out vec4 color;

    void main() {
        color = vec4(1); // vec4(1) is white.
    }
)";

void draw() {
    /*
     * Drawing code.
     */

    // Vertex data. This contains the 2D positions for a triangle pointing down.
    float tri[] = {
        -1, 1, 0, -1, 1, 1
    };

    // This creates a buffer object in OpenGL that contains the vertex data we specified.
    GLuint buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(tri), tri, GL_DYNAMIC_DRAW);

    // This tells OpenGL we want to use our buffer for attribute 0 (the one we specified in our shader!).
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

    // We tell OpenGL we want to draw triangles, starting from the beginning of the data, and drawing 3 vertices.
    // This draws our triangle.
    glDrawArrays(GL_TRIANGLES, 0, 3);

    // Clean-up.
    glDisableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, &buffer);
}

int main(int argc, char** argv) {
    /*
     * Initializing our program.
     */

    // Initializing GLFW. This is the library that lets us create a window.
    glfwInit();

    // This tells GLFW we want our window to use OpenGL 4.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    
    // This creates our window! It tells GLFW we want our window to be 960x640, and be named "Tutorial".
    // And we also tell GLFW that this is the window we want to be active.
    GLFWwindow* win = glfwCreateWindow(960, 640, "Tutorial", nullptr, nullptr);
    glfwMakeContextCurrent(win);

    // This loads OpenGL's functions for our window.
    gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

    // This tells OpenGL to make the background color black.
    glClearColor(0, 0, 0, 1);

    // Some basic setup. This stuff isn't essential. Don't worry about it.
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_TEXTURE_2D);
    glShadeModel(GL_SMOOTH);

    // This creates a "vertex array". Don't worry about it, they're not very useful, but we need this
    // for our program to work correctly.
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    /*
     * Loading shaders.
     */

    // Create two OpenGL shader objects, one vertex and one fragment.
    GLuint vsh = glCreateShader(GL_VERTEX_SHADER), fsh = glCreateShader(GL_FRAGMENT_SHADER);

    // Attach the source code written in the VERTEX_SHADER and FRAGMENT_SHADER variables to the shaders.
    const char* sources[] = { VERTEX_SHADER, FRAGMENT_SHADER };
    GLint lengths[] = { strlen(VERTEX_SHADER), strlen(FRAGMENT_SHADER) };
    glShaderSource(vsh, 1, &sources[0], &lengths[0]);
    glShaderSource(fsh, 1, &sources[1], &lengths[1]);

    // Compile the shaders from their source code.
    glCompileShader(vsh);
    glCompileShader(fsh);

    // Check for compiler errors.
    int vstatus, fstatus;
    glGetShaderiv(vsh, GL_COMPILE_STATUS, &vstatus);
    glGetShaderiv(fsh, GL_COMPILE_STATUS, &fstatus);
    if (!vstatus) {
        char buffer[512];
        glGetShaderInfoLog(vsh, 512, nullptr, buffer);
        cout << "Vertex shader error: " << endl << buffer << endl;
    }
    if (!fstatus) {
        char buffer[512];
        glGetShaderInfoLog(vsh, 512, nullptr, buffer);
        cout << "Fragment shader error: " << endl << buffer << endl;
    }

    // Create a shader program.
    GLuint prog = glCreateProgram();

    // Attach our shaders to the program.
    glAttachShader(prog, vsh);
    glAttachShader(prog, fsh);

    // Link the shaders to the program - now the program will have the vertex and fragment shaders we wrote all connected up.
    glLinkProgram(prog);
    
    // Tell OpenGL we want to draw using the shader program we just created.
    glUseProgram(prog);

    /*
     * Main loop.
     */

    // We continue to re-render as long as the window is supposed to stay alive.
    // glfwWindowShouldClose(win) will return false when the window's X button is clicked.
    while (!glfwWindowShouldClose(win)) {
        // Clear the screen for the new frame.
        glClear(GL_COLOR_BUFFER_BIT);

        // Check for events like key presses.
        glfwPollEvents();

        // Call our drawing code for this frame.
        draw();

        // Put all the stuff we drew onto the screen.
        glfwSwapBuffers(win);
    }

    // Cleaning up our window.
    glfwDestroyWindow(win);
}
